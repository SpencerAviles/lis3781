-- MS SQL Server


SET ANSI_WARNINGS ON;
GO

USE master;
GO

IF EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE name  = N'spa17b')
DROP DATABASE spa17b;
GO

IF NOT EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE name = N'spa17b')
CREATE DATABASE spa17b;
GO

USE spa17b;
GO


-- Person Table

IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
 per_id SMALLINT NOT NULL IDENTITY(1,1),
 per_ssn BINARY(64) NULL,
 per_salt BINARY(64) NULL,
 per_fname VARCHAR(15) NOT NULL,
 per_lname VARCHAR(30) NOT NULL,
 per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m', 'f')),
 per_dob DATE NOT NULL,
 per_street VARCHAR(30) NOT NULL,
 per_city VARCHAR(30) NOT NULL,
 per_state CHAR(2) NOT NULL,
 per_zip INT NOT NULL CHECK (per_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 per_email VARCHAR(100) NULL,
 per_type CHAR(1) NOT NULL CHECK (per_type IN('c', 's')),
 per_notes VARCHAR(45) NULL,
 PRIMARY KEY (per_id),

 CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)

);
GO 


-- Phone Table
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
 phn_id SMALLINT NOT NULL IDENTITY(1,1),
 per_id SMALLINT NOT NULL,
 phn_num BIGINT NOT NULL CHECK (phn_num LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 phn_type CHAR(1) NOT NULL CHECK (phn_type IN('h','c','w','f')),
 phn_notes VARCHAR(255) NULL,
 PRIMARY KEY (phn_id),

 	 CONSTRAINT fk_phone_person
	 FOREIGN KEY (per_id)
	 REFERENCES dbo.person (per_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE
);
GO

-- Customer Table
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
 per_id SMALLINT NOT NULL,
 cus_balance DECIMAL(7,2) NOT NULL CHECK (cus_balance >= 0),
 cus_tot_sales DECIMAL(7,2) NOT NULL CHECK (cus_tot_sales >= 0),
 cus_notes VARCHAR(45) NULL,
 PRIMARY KEY (per_id),

 	 CONSTRAINT fk_customer_person
	 FOREIGN KEY (per_id)
	 REFERENCES dbo.person (per_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE
);
GO

-- Slsrep Table

IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
 per_id SMALLINT NOT NULL,
 srp_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (srp_yr_sales_goal >= 0),
 srp_ytd_sales DECIMAL(8,2) NOT NULL CHECK (srp_ytd_sales >= 0),
 srp_ytd_comm DECIMAL (7,2) NOT NULL CHECK (srp_ytd_comm >= 0),
 srp_notes VARCHAR(45) NULL,
 PRIMARY KEY (per_id),

 	 CONSTRAINT fk_slsrep_person
	 FOREIGN KEY (per_id)
	 REFERENCES dbo.person (per_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Srp_hist Table

IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
 sht_id SMALLINT NOT NULL IDENTITY(1,1),
 per_id SMALLINT NOT NULL,
 sht_type CHAR(1) NOT NULL CHECK (sht_type IN('i','u','d')),
 sht_modified DATETIME NOT NULL,
 sht_modifier VARCHAR(45) NOT NULL DEFAULT system_user,
 sht_date DATE NOT NULL DEFAULT getDate(),
 sht_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (sht_yr_sales_goal >= 0),
 sht_yr_total_sales DECIMAL(8,2) NOT NULL CHECK (sht_yr_total_sales >= 0),
 sht_yr_total_comm DECIMAL(7,2) NOT NULL CHECK (sht_yr_total_comm >= 0),
 sht_notes VARCHAR(45) NULL,
 PRIMARY KEY (sht_id),

 	 CONSTRAINT fk_srp_hist_slsrep
	 FOREIGN KEY (per_id)
	 REFERENCES dbo.slsrep (per_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Contact Table
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
 cnt_id INT NOT NULL IDENTITY(1,1),
 per_cid SMALLINT NOT NULL,
 per_sid SMALLINT NOT NULL,
 cnt_date DATETIME NOT NULL,
 cnt_notes VARCHAR(255) NULL,
 PRIMARY KEY (cnt_id),

 	 CONSTRAINT fk_contact_customer
	 FOREIGN KEY (per_cid)
	 REFERENCES dbo.customer (per_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE,


	 CONSTRAINT fk_contact_slsrep
	 FOREIGN KEY (per_sid)
	 REFERENCES dbo.slsrep (per_id)
	 ON DELETE NO ACTION 
	 ON UPDATE NO ACTION

);
GO

-- [order] Table

IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
 ord_id INT NOT NULL IDENTITY(1,1),
 cnt_id INT NOT NULL,
 ord_placed_date DATETIME NOT NULL,
 ord_filled_date DATETIME NULL,
 ord_notes VARCHAR(255) NULL,
 PRIMARY KEY (ord_id),

 	 CONSTRAINT fk_order_contact
	 FOREIGN KEY (cnt_id)
	 REFERENCES dbo.contact (cnt_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Store Table


IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
 str_id SMALLINT NOT NULL IDENTITY(1,1),
 str_name VARCHAR(45) NOT NULL,
 str_street VARCHAR(30) NOT NULL,
 str_city VARCHAR(30) NOT NULL,
 str_state CHAR(2) NOT NULL DEFAULT 'FL',
 str_zip INT NOT NULL CHECK (str_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 str_phone BIGINT NOT NULL CHECK (str_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 str_email VARCHAR(100) NOT NULL,
 str_url VARCHAR(100) NOT NULL,
 str_notes VARCHAR(255) NULL,
 PRIMARY KEY (str_id),
);
GO

-- Invoice Table

IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
 inv_id INT NOT NULL IDENTITY(1,1),
 ord_id INT NOT NULL,
 str_id SMALLINT NOT NULL,
 inv_date DATETIME NOT NULL,
 inv_total DECIMAL(8,2) NOT NULL CHECK (inv_total >= 0),
 inv_paid BIT NOT NULL,
 inv_notes VARCHAR(255) NULL,
 PRIMARY KEY (inv_id),

 	 CONSTRAINT ux_ord_id UNIQUE nonclustered (ord_id ASC),

	 CONSTRAINT fk_invoice_order
	 FOREIGN KEY (ord_id)
	 REFERENCES dbo.[order](ord_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE,

	 CONSTRAINT fk_invoice_store
	 FOREIGN KEY (str_id)
	 REFERENCES dbo.store (str_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Payment Table

IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
 pay_id INT NOT NULL IDENTITY(1,1),
 inv_id INT NOT NULL,
 pay_date DATETIME NOT NULL,
 pay_amt DECIMAL(7,2) NOT NULL CHECK (pay_amt >= 0),
 pay_notes VARCHAR(255) NULL,
 PRIMARY KEY (pay_id),

 	 CONSTRAINT fk_payment_invoice
	 FOREIGN KEY (inv_id)
	 REFERENCES dbo.invoice (inv_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Vendor Table

IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
 ven_id SMALLINT NOT NULL IDENTITY(1,1),
 ven_name VARCHAR(45) NOT NULL,
 ven_street VARCHAR(30) NOT NULL,
 ven_city VARCHAR(30) NOT NULL,
 ven_state CHAR(2) NOT NULL DEFAULT 'FL',
 ven_zip INT NOT NULL CHECK (ven_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 ven_phone BIGINT NOT NULL CHECK (ven_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 ven_email VARCHAR(100) NULL,
 ven_url VARCHAR(100) NULL,
 ven_notes VARCHAR(255),
 PRIMARY KEY (ven_id)
);
GO

-- Product Table

IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
 pro_id SMALLINT NOT NULL IDENTITY(1,1),
 ven_id SMALLINT NOT NULL,
 pro_name VARCHAR(30) NOT NULL,
 pro_descript VARCHAR(45) NULL,
 pro_weight FLOAT NOT NULL CHECK (pro_weight >= 0),
 pro_qoh SMALLINT NOT NULL CHECK (pro_qoh >= 0),
 pro_cost DECIMAL(7,2) NOT NULL CHECK (pro_cost >= 0),
 pro_price DECIMAL(7,2) NOT NULL CHECK (pro_price >= 0),
 pro_discount DECIMAL(3,0) NULL,
 pro_notes VARCHAR(255) NULL,
 PRIMARY KEY (pro_id),

 	 CONSTRAINT fk_product_vendor
	 FOREIGN KEY (ven_id)
	 REFERENCES dbo.vendor (ven_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- Product_hist Table

IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
 pht_id INT NOT NULL IDENTITY(1,1),
 pro_id SMALLINT NOT NULL,
 pht_date DATETIME NOT NULL,
 pht_cost DECIMAL(7,2) NOT NULL CHECK (pht_cost >= 0),
 pht_price DECIMAL(7,2) NOT NULL CHECK (pht_price >= 0),
 pht_discount DECIMAL(3,0) NULL,
 pht_notes VARCHAR(255) NULL,
 PRIMARY KEY (pht_id),

 	 CONSTRAINT fk_product_hist_product
	 FOREIGN KEY (pro_id)
	 REFERENCES dbo.product (pro_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE
);
GO

-- Order_line Table

IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
 oln_id INT NOT NULL IDENTITY(1,1),
 ord_id INT NOT NULL,
 pro_id SMALLINT NOT NULL,
 oln_qty SMALLINT NOT NULL CHECK (oln_qty >= 0),
 oln_price DECIMAL(7,2) NOT NULL CHECK (oln_price >= 0),
 oln_notes VARCHAR(255) NULL,
 PRIMARY KEY (oln_id),

 	 CONSTRAINT fk_order_line_order
	 FOREIGN KEY (ord_id)
	 REFERENCES dbo.[order] (ord_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE,

	 CONSTRAINT fk_order_line_product
	 FOREIGN KEY (pro_id)
	 REFERENCES dbo.product (pro_id)
	 ON DELETE CASCADE
	 ON UPDATE CASCADE

);
GO

-- SELECT * FROM information_schema.tables;

-- Data for Person Table

INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm','1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 983208440, 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', 's', NULL),
(4, NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 132084409, 'jthompson@gamil.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL),
(6, NULL, 'Tony', 'Smith', 'm', '1972-05-04', '332 Palm Avenue', 'Mailbu', 'CA', 902638332, 'tsmith@yahoo.com', 'c', NULL),
(7, NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown Street', 'Clevland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9, NULL, 'Sandra', 'Smith', 'f', '1990-01-26', '87912 Lawrence Avenue', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@sympatico.com', 'c', NULL),
(13, NULL, 'Adam', 'Smith', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@sympatico.com', 'c', NULL),
(15, NULL, 'Bill', 'Neiderheim', 'm', '1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', 'c', NULL);
GO

-- SELECT * FROM dbo.person

-- Data for Slsrep Table

INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, 'Great salesperson'),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);
GO

-- SELECT * FROM dbo.slsrep;

-- Data for Customer Tables

INSERT INTO dbo.customer
(per_id, cus_balance,  cus_tot_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, 'Customer always pays on time'),
(9, 981.73, 1672.38, 'High balance'),
(10, 251.02, 13782.96, 'Good Customer');
GO

-- SELECT * FROM dbo.customer;

-- Data for Contact Table

INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-09-29', NULL),
(3, 7, '2002-08-15', NULL),
(2, 7, '2002-09-01', NULL),
(4, 7, '2004-01-05', NULL);
GO

-- SELECT * FROM dbo.contact;

-- Data for Order Table

INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-24', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL);
GO

-- SELECT * FROM dbo.[order];

-- Data for Store Table

INSERT INTO dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
('Walgreens', '14567 Walnut Ln', 'Aspen', 'IL', '475315690', '3127658127', 'info@walgreens.com', 'http://www.walgreens.com', NULL),
('CVS', '572 Casper Rd', 'Chicago', 'IL', '505231519', '3128926534', 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger'),
('Lowes', '81309 Catapult Ave', 'Clover', 'WA', '802345671', '9017653421', 'sales@lowes.com', 'http://www.lowes.com', NULL),
('Walmart', '14567 Walnut Ln', 'St. Louis', 'FL', '387563628', '8722718923', 'info@walmart.com', 'http://www.walmart.com', NULL),
('Dollar General', '47583 Davison Rd', 'Detroit', 'MI', '482983456', '3137583492', 'ask@dollargeneral.com', 'http://www.dollargeneral.com', 'recently sold property');
GO

-- SELECT * FROM dbo.store;

-- Data for Invoice Table

INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2006-11-11', 100.59, 0, NULL),
(1, 1, '2010-09-16', 57.34, 0, NULL),
(3, 2, '2011-01-10', 99.32, 1, NULL),
(2, 3, '2008-06-24', 1109.67, 1, NULL);
GO

-- SELECT * FROM dbo.invoice;

-- Data for Vendor Table

INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electric', '100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'Very good turnaround'),
('Cisco', '300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear', '100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone'),
('Snap-On', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!');
GO

-- SELECT * FROM dbo.vendor;

-- Data for Product Table

INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail', '16 Gallon', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale');
GO

-- SELECT * FROM dbo.product;

-- Data for Order_line Table

INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(4, 5, 13, 58.99, NULL);
GO

-- SELECT * FROM dbo.order_line;

-- Data for Payment Table

INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, NULL),
(4, '2010-09-28', 4.99, NULL),
(1, '2008-07-23', 8.75, NULL),
(3, '2010-10-31', 19.55, NULL),
(2, '2011-03-29', 32.5, NULL);
GO

-- SELECT * FROM dbo.payment;

-- Data for Product_hist Table

INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price');
GO

-- SELECT * FROM dbo.product_hist;

-- Data for Srp_hist table
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);
GO

-- SELECT * FROM dbo.srp_hist;

-- Data for Phone Table

INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(1, '4565892132', 'c', 'Personal phone'),
(2, '5502123546', 'w', 'Work phone'),
(3, '4562313977', 'w', NULL),
(4, '7895232145', 'f', 'Fax'),
(5, '6512489735', 'h', NULL);
GO

-- SELECT * FROM dbo.phone;


-- Create Person SSN
CREATE PROC dbo.CreatePersonSSN
AS
BEGIN
	DECLARE @salt binary(64);
	DECLARE @ran_num int;
	DECLARE @ssn binary(64);
	DECLARE @x INT, @y INT;
	SET @x = 1;

	SET @y=(select count(*) from dbo.person);

	WHILE (@x <= @y)
	BEGIN

	SET @salt=CRYPT_GEN_RANDOM(64);
	SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
	SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

	UPDATE dbo.person
	SET per_ssn=@ssn, per_salt=@salt
	WHERE per_id=@x;

	SET @x = @x + 1;
	END;
END;
GO

exec dbo.CreatePersonSSN

select * from dbo.person;
go

-- SQL Statments

-- 1
IF OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') IS NOT NULL
DROP VIEW dbo.v_paid_invoice_total;
GO

CREATE VIEW dbo.v_paid_invoice_total AS
       SELECT p.per_id per_fname, per_lname, sum(inv_total) as sum_total, FORMAT(sum(inv_total), 'C', 'en-us') as paid_invoice_total
       FROM dbo.person p
        JOIN dbo.customer c on p.per_id=c.per_id
	JOIN dbo.contact ct on c.per_id=ct.per_id
	JOIN dbo.[order] o on ct.cnt_id=o.cnt_id
	JOIN dbo.invoice i on o.ord_id=i.ord_id
       WHERE inv_paid != 0
       GROUP BY p.per_id, per_fname, per_lname
GO
SELECT per_id, per_fname, per_lname, paid_invoice_total
FROM dbo.v_paid_invoice_total
ORDER BY sum_total DESC;
GO

drop view dbo.v_paid_invoice_total;

-- 2
