> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 Advanced Database Management

## Spencer Aviles 

### Project 1 Requirements:

1. Create a person table with at least 15 records
2. Create tables for clients, attorneys, and judges
3. Create a bar and specialty table
4. Create a case and court table
5. Create a phone table
6. Create a judge history table
7. All tables, besides the person table, must have at least 5 records 

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshots of SQL code for reports 
* Links to .mwb and .sql files 


#### Assignment Screenshots:

*Screenshots of SQL Code:*

![ERD Screenshot](erd.png)

*Screenshots of Reports SQL Code*

*1*

![report 1](report1.png)

*2*

![report 2](report2.png)

*3*

![report 3](report3.png)

*4*

![report 4](report4.png)

*5*

![report 5](report5.png)

*6*

![report 6](report6.png)

*Extra Credit*

![extra credit](extracredit.png)



*SQL and MWB files*

[SQL file](docs/lis3781_p1_solutions.sql "SQL Code")
[MWB file](docs/LIS3781_p1_solutions.mwb "MWB File")

