> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 Advanced Database Management

## Spencer Aviles 

### Project 2 Requirements:

1. Install MongoDB and all of the necessary files
2. Complete all queries
3. Answer the Chapter 16 and MongoDB questions 

#### README.md file should include the following items:

* Screenshot of MongoDB shell command
* Screenshot of JSON code for queries 

#### Assignment Screenshots:

*Screenshots of Mongo Command:*

![Mongo cmd](show.png)


