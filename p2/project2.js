//1 Display all documents
db.restaurants.find()

//1b Returns one document that satisfies query criteria
db.restaurants.findOne()

//2 Display the number of documents in the collection
db.restaurants.find().count()

//3 Retrieve first 5 documents
db.restaurants.find().limit(5);

//4 Retrieve restaurants in Brooklyn borough
db.restaurants.find({ "borough": "Brooklyn"})

//4b
db.restaurants.find({ "borough": "Brooklyn"}).count()

//5 Retrieve restaurants whose cuisine is American
db.restaurants.find({ "cuisine": "American "})
//5b using regex
db.restaurants.find({ "cuisine": /^American\s$/ })

//6 Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers
db.restaurants.find({ "borough": "Manhattan", "cuisine": "Hamburgers" })

//7 Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers


//8 Query zipcode field in embedded address document
