> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 Advanced Database Management

## Spencer Aviles 

### Assignment 3 Requirements:

1. Log into Oracle Server using RemoteLabs
2. Create and populate Oracle tables

#### README.md file should include the following items:

* Screenshot of SQL Code
* Screenshot of Populated Tables w/n the Oracle Environment
* Optional: SQL code for the required reports
* Bitbucket repo link


#### Assignment Screenshots:

*Screenshots of SQL Code:*

![SQL code for Screenshot](sql_code_1.png) ![Part 2](sql_code_2.png)

*Screenshot of Populated Tables*

*Customer*

![Output for customer Screenshot](pop_cus_table.png)

*Commodity*

![Output for commodity Screenshot](pop_com_table.png)

*Orders*

![Output for orders Screenshot](pop_ord_table.png)


