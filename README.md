> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Spencer Aviles

### LIS3781  Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Ampps
    - Provide screenshots of installations
    - Create Bitbucket Repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
    - Create an ERD and provide screenshots

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - A2 Bitbucket requirements will be put w/in A1
    - Create two tables (company and customer) locally and remotely
    - Populate these tables and hash the SSN of the customers
    - Grant 2 users privileges on local server

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Log into Oracle using RemoteLabs
    - Create and populate 3 tables (customer, commodity, order)
    - Complete the SQL reports 

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Log into MS SQL Server using RemoteLabs
    - Populate person table with 10 records
    - Populate each other tables with at least 5 records
    - Complete the SQL reports 

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Log into MS SQL Server using RemoteLabs
    - Populate sale table with 25 records
    - Populate other tables with at least 5 records
    - Complete SQL Reports 

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create person table with 15 records
    - Populate the rest of the tables with at least 5 records
    - Create an ERD and provide screenshots
    - Provide screenshots of SQL code for reports

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install MongoDB
    - Complete all the queries
    - Provide screenshots of answers to query questions
