> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 Advanced Database Management

## Spencer Aviles 

### Assignment 2 Requirements:

1. Locally: Create spa17b database and two tables, company and customer. Both tables must be populated
2. Remotely: Create company and customer tables. Both most be populated
3. Create 2 users on local server and grant privileges 

#### README.md file should include the following items:

* Screenshots of the SQL code to create and populate tables 
* Screenshot of populated tables


#### Assignment Screenshots:

*Screenshot of Company table code:*

![SQL code for company Screenshot](company_table.png)

*Screenshot of Customer table code:*

*Part 1*

![First part of SQL code for customer Screenshot](customer_1.png)

*Part 2*

![Second part of SQL code for customer Screenshot](customer_2.png)

*Screenshot of populated tables:*

![Populated tables Screenshot](pop_tables.png)


