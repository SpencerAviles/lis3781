> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 Advanced Database Management

## Spencer Aviles 

### Assignment 5 Requirements:

1. Log into MS SQL Server using RemoteLabs
2. Create and populate tables

#### README.md file should include the following items:

* Screenshot of ERD
* Links to SQL code

#### Assignment Screenshots:

*Screenshots of ERD:*

![Screenshot of ERD](ERD.png)

*SQL file*
[SQL File](docs/lis3781_a5_solutions.sql)



