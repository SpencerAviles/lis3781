> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management

## Spencer Aviles

### Assignment 1 Requirements:

1. Distributed Version Control with GIT and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram and SQL Code
5. Bitbucket Repo links:
   a)this assignment and
   b)the completed tutorial (bitbucketlocations)

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex.1 of SQL Solution
* git commands with short descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository or reinitializes an existing one
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of A1 ERD:

![A1 ERD Screenshot](erd_a1.png)

*Screenshot of Ampps Installation
![Ampps Install Screenshot](ampps.png)

*Screenshot of A1 Ex1
![A1 Ex1 Screenshot from Putty](QueryA1Ex1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/SpencerAviles/bitbucketstationlocations/src/master)

